<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="File Store Drive">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <title>File Store - Drive</title>

    <link rel="stylesheet" href="/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-brands.css">
    <link rel="stylesheet" href="/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-regular.css">
    <link rel="stylesheet" href="/vendor/@fortawesome/fontawesome-free-webfonts/css/fa-solid.css">
    <link rel="stylesheet" href="/vendor/@fortawesome/fontawesome-free-webfonts/css/fontawesome.css">
    <link rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="/vendor/animate.css/animate.css">
    <link rel="stylesheet" href="/vendor/whirl/dist/whirl.css">
    <link rel="stylesheet" href="/css/bootstrap.css" id="bscss">
    <link rel="stylesheet" href="/css/app.css" id="maincss">
</head>

<body>
<div class="wrapper">
    <header class="topnavbar-wrapper">
        <nav class="navbar topnavbar">
            <div class="navbar-header">
                <a class="navbar-brand" href="#/">
                    <div class="brand-logo">
                        <img class="img-fluid" src="/img/logo.png" alt="FileStore Logo">
                    </div>
                    <div class="brand-logo-collapsed">
                        <img class="img-fluid" src="/img/logo-single.png" alt="FileStore Logo">
                    </div>
                </a>
            </div>
            <ul class="navbar-nav mr-auto flex-row">
                <li class="nav-item">
                    <a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed">
                        <em class="fas fa-bars"></em>
                    </a>
                    <a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true">
                        <em class="fas fa-bars"></em>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <#assign section="invoices">
    <#include "menu.ftl">
    <section class="section-container">
        <div class="content-wrapper">
            <div class="content-heading">
                <div>
                    <div class="btn-group" role="group">
                        <button class="btn btn-secondary" type="button">
                            <a href="/api/files"><em class="fa fa-home"></em></a>
                        </button>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped w-100" id="filestable">
                        <thead>
                        <tr>
                            <th data-priority="1">Date</th>
                            <th>Montant</th>
                            <th>Charge ID</th>
                            <th>Product ID</th>
                            <th>Email</th>
                            <th>Télécharger</th>
                        </tr>
                        </thead>
                        <tbody>
                        <#list content.invoices as item>
                        <tr>
                            <td>${item.creationDate}</td>
                            <td>${item.amount}</td>
                            <td>${item.charge_id}</td>
                            <td>${item.product_id}</td>
                            <td>${item.customer_email}</td>
                            <td><a href="${item.invoice_pdf}" class="mr-3"><i class="fas fa-download"></i></a></td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="/vendor/modernizr/modernizr.custom.js"></script>
<script src="/vendor/jquery/dist/jquery.js"></script>
<script src="/vendor/popper.js/dist/umd/popper.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/vendor/js-storage/js.storage.js"></script>
<script src="/vendor/jquery.easing/jquery.easing.js"></script>
<script src="/vendor/animo/animo.js"></script>
<script src="/vendor/screenfull/dist/screenfull.js"></script>
<script src="/js/app.js"></script>
<script>
    $('#file').change(function() {
        console.log("called");
        var filename = $('#file').val().replace(/\\/g, '/').replace(/.*\//, '');
        console.log(filename);
        $('#filename').val(filename);
    });
</script>
</body>
</html>