package fr.miage.filestore.subscriptions.service;

import fr.miage.filestore.subscriptions.entities.Plan;

import java.util.ArrayList;

public interface PlansService {

    ArrayList<Plan> getPlans();

    Plan getByStripeId(String stripeId);

    Plan getByPriceId(String priceId);

    Plan getPlanByEnum(String plan);
}
