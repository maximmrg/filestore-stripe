package fr.miage.filestore.subscriptions.service;

import fr.miage.filestore.subscriptions.entities.EnumSubPlans;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

public interface SubscriptionService {

    SubscriptionProfile connectedSubProfile();

    SubscriptionProfile get(String id);

    SubscriptionProfile getByEmail(String email);

    void updateSubscription(String customerEmail, EnumSubPlans valueOf, boolean b);

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    void incrementQuota(String customerEmail, long mo);

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    void decrementQuota(String customerEmail, long mo);
}
