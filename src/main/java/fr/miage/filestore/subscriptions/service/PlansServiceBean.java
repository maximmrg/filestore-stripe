package fr.miage.filestore.subscriptions.service;

import fr.miage.filestore.subscriptions.entities.Plan;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class PlansServiceBean implements PlansService {

    private static final Logger LOGGER = Logger.getLogger(PlansServiceBean.class.getName());

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;


    @Override
    public ArrayList<Plan> getPlans() {
        List<Plan> plans = em.createQuery("from Plan", Plan.class).getResultList();
        return new ArrayList<>(plans);
    }

    @Override
    public Plan getByStripeId(String stripeId) {
        TypedQuery<Plan> q = em.createQuery("SELECT b FROM Plan b WHERE b.stripeId = :stripeId", Plan.class);
        q.setParameter("stripeId", stripeId);
        return q.getSingleResult();
    }

    @Override
    public Plan getByPriceId(String priceId) {
        TypedQuery<Plan> q = em.createQuery("SELECT b FROM Plan b WHERE b.id = :id", Plan.class);
        q.setParameter("id", priceId);
        return q.getSingleResult();
    }

    @Override
    public Plan getPlanByEnum(String plan) {
        TypedQuery<Plan> q = em.createQuery("SELECT b FROM Plan b WHERE b.enumPlan = :enumPlan", Plan.class);
        q.setParameter("enumPlan", plan);
        return q.getSingleResult();
    }

}
