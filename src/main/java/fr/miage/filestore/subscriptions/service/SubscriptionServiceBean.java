package fr.miage.filestore.subscriptions.service;

import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.subscriptions.entities.EnumSubPlans;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class SubscriptionServiceBean implements SubscriptionService {

    private static final Logger LOGGER = Logger.getLogger(SubscriptionService.class.getName());

    @EJB
    AuthenticationService auth;

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;


    @Override
    public SubscriptionProfile get(String id) {
        SubscriptionProfile item = em.find(SubscriptionProfile.class, id);
        return item;
    }

    @Override
    public SubscriptionProfile getByEmail(String email) {
        TypedQuery<SubscriptionProfile> q = em.createQuery("SELECT b FROM SubscriptionProfile b WHERE b.email = :email", SubscriptionProfile.class);
        q.setParameter("email", email);
        return q.getSingleResult();
    }

    @Override
    public SubscriptionProfile connectedSubProfile() {
        LOGGER.log(Level.INFO, "Getting subscription connected profile");
        String connectedId = auth.getConnectedProfile().getId();
        LOGGER.log(Level.INFO, "Retrieving profile for " + connectedId);
        SubscriptionProfile subProfile = this.get(connectedId);;

        if(subProfile == null){
            LOGGER.log(Level.INFO, "Creating a subscription profile for " + connectedId);
            subProfile = new SubscriptionProfile();
            subProfile.setPlan(EnumSubPlans.FREE);
            subProfile.setQuota(0);
            subProfile.setId(connectedId);
            subProfile.setEmail(auth.getConnectedProfile().getEmail());
            em.persist(subProfile);
        }

        System.out.println("Subscribed profile: " + subProfile);

        return subProfile;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void updateSubscription(String customerEmail, EnumSubPlans valueOf, boolean b) {
        SubscriptionProfile subProfile = getByEmail(customerEmail);
        subProfile.setPlan(valueOf);
        subProfile.setSubscribed(b);
        em.persist(subProfile);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void incrementQuota(String customerEmail, long mo){
        SubscriptionProfile subProfile = getByEmail(customerEmail);
        subProfile.incrementQuota(mo);
        em.persist(subProfile);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void decrementQuota(String customerEmail, long mo){
        SubscriptionProfile subProfile = getByEmail(customerEmail);
        subProfile.decrementQuota(mo);
        em.persist(subProfile);
    }
}
