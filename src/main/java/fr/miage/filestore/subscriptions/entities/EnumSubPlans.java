package fr.miage.filestore.subscriptions.entities;

public enum EnumSubPlans {
    FREE,
    MEDIUM,
    LARGE
}
