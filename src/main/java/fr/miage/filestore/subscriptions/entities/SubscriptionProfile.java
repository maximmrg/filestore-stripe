package fr.miage.filestore.subscriptions.entities;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

@Entity
public class SubscriptionProfile implements Serializable {

    @Id
    @JsonbProperty
    private String id;
    private String email;
    private boolean subscribed;
    private long quota;
    private EnumSubPlans plan;

    public SubscriptionProfile() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public long getQuota() {
        return quota;
    }

    public void setQuota(long quota) {
        this.quota = quota;
    }

    public EnumSubPlans getPlan() {
        return plan;
    }

    public void setPlan(EnumSubPlans plan) {
        this.plan = plan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void incrementQuota(long mo) {
        this.quota = this.quota+mo;
    }

    public void decrementQuota(long mo) {
        this.quota = this.quota-mo;
        if(quota < 0) quota = 0;
    }
}
