package fr.miage.filestore.subscriptions.entities;

import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Plan implements Serializable {

    @Id
    @JsonbProperty
    private String id;
    private String name;
    private String description;
    private double price;
    private long authorizedQuota;
    private String enumPlan;
    private String stripeId;

    public Plan() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getAuthorizedQuota() {
        return authorizedQuota;
    }

    public void setAuthorizedQuota(long authorizedQuota) {
        this.authorizedQuota = authorizedQuota;
    }

    public String getPlan() {
        return enumPlan;
    }

    public void setPlan(String plan) {
        this.enumPlan = plan;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStripeId() {
        return stripeId;
    }

    public void setStripeId(String stripeId) {
        this.stripeId = stripeId;
    }
}
