package fr.miage.filestore.stripe.resources;

import com.stripe.Stripe;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.PaymentIntent;
import com.stripe.model.StripeObject;
import com.stripe.model.checkout.Session;
import com.stripe.net.Webhook;
import com.stripe.param.checkout.SessionCreateParams;
import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.api.template.Template;
import fr.miage.filestore.api.template.TemplateContent;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.config.FileStoreConfig;
import fr.miage.filestore.file.FileServiceException;
import fr.miage.filestore.stripe.entities.StripeCustomer;
import fr.miage.filestore.stripe.services.CustomerService;
import fr.miage.filestore.stripe.services.StripeService;
import fr.miage.filestore.subscriptions.entities.EnumSubPlans;
import fr.miage.filestore.subscriptions.entities.Plan;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;
import fr.miage.filestore.subscriptions.service.PlansService;
import fr.miage.filestore.subscriptions.service.SubscriptionService;
import retrofit2.http.Body;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("stripe")
@OnlyOwner
public class PaymentResource {

    private static final Logger LOGGER = Logger.getLogger(PaymentResource.class.getName());

    @EJB
    private AuthenticationService auth;

    @EJB
    private SubscriptionService sub;

    @EJB
    private StripeService stripe;

    @EJB
    private PlansService plan;

    @EJB
    private CustomerService customerService;

    @Context
    UriInfo uri;

    @Inject
    private FileStoreConfig config;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("payment/{id}")
    public Response payment(@PathParam("id") String id) throws StripeException {
        LOGGER.log(Level.INFO, "GET /api/stripe/payment/" + id);

        Stripe.apiKey = config.stripeApiKey();

        Plan p = plan.getByPriceId(id);

        StripeCustomer customer = null;

        try{
            customer = customerService.getStripeCustomerByFilestoreId(auth.getConnectedProfile().getId());
        }catch (NoResultException exception){
            LOGGER.log(Level.INFO, "New customer detected.");
        }


        String YOUR_DOMAIN = uri.getBaseUri().toString();
        SessionCreateParams params =
                SessionCreateParams.builder()
                        .setMode(SessionCreateParams.Mode.SUBSCRIPTION)
                        .setSuccessUrl(YOUR_DOMAIN + "stripe/success")
                        .setCancelUrl(YOUR_DOMAIN + "stripe/cancel")
                        .setCustomer(customer == null ? null : customer.getId())
                        .setCustomerEmail(customer == null ? auth.getConnectedProfile().getEmail() : null)
                        .addLineItem(
                                SessionCreateParams.LineItem.builder()
                                        .setQuantity(1L)
                                        .setPrice(id)
                                        .build())
                        .build();
        Session session = Session.create(params);
        LOGGER.log(Level.INFO, "Checkout: " + URI.create(session.getUrl()));
        return Response.temporaryRedirect(URI.create(session.getUrl())).build();
    }

    @GET
    @Template(name = "cancel")
    @Path("cancel")
    @Produces({MediaType.TEXT_HTML})
    public TemplateContent cancel() throws FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/stripe/cancel (html)");
        TemplateContent<Map<String, Object>> content = new TemplateContent<>();
        Map<String, Object> value = new HashMap<>();
        value.put("profile", auth.getConnectedProfile());
        value.put("subscription", sub.connectedSubProfile());
        SubscriptionProfile subProfile = sub.connectedSubProfile();
        value.put("plan", plan.getPlanByEnum(subProfile.getPlan().toString()));
        content.setContent(value);
        return content;
    }

    @GET
    @Template(name = "success")
    @Path("success")
    @Produces({MediaType.TEXT_HTML})
    public TemplateContent success() throws FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/stripe/success (html)");
        TemplateContent<Map<String, Object>> content = new TemplateContent<>();
        Map<String, Object> value = new HashMap<>();
        value.put("profile", auth.getConnectedProfile());
        value.put("subscription", sub.connectedSubProfile());
        SubscriptionProfile subProfile = sub.connectedSubProfile();
        value.put("plan", plan.getPlanByEnum(subProfile.getPlan().toString()));
        content.setContent(value);
        return content;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("portal")
    public Response clientPortal() throws StripeException {

        String customerId = customerService.getStripeCustomerByFilestoreId(auth.getConnectedProfile().getId()).getId();
        LOGGER.log(Level.INFO, "GET /api/stripe/portal/" + customerId);

        Stripe.apiKey = config.stripeApiKey();

        com.stripe.param.billingportal.SessionCreateParams params;

        params = com.stripe.param.billingportal.SessionCreateParams.builder()
                .setCustomer(customerId)
                .setReturnUrl(uri.getBaseUri().toString() + "plans")
                .build();

        com.stripe.model.billingportal.Session session = com.stripe.model.billingportal.Session.create(params);

        return Response.temporaryRedirect(URI.create(session.getUrl())).build();
    }

}
