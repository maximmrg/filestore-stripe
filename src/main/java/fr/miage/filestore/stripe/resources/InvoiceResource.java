package fr.miage.filestore.stripe.resources;

import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.api.template.Template;
import fr.miage.filestore.api.template.TemplateContent;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.FileServiceException;
import fr.miage.filestore.stripe.services.InvoiceService;
import fr.miage.filestore.subscriptions.entities.EnumSubPlans;
import fr.miage.filestore.subscriptions.entities.Plan;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;
import fr.miage.filestore.subscriptions.service.PlansService;
import fr.miage.filestore.subscriptions.service.SubscriptionService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("invoices")
@OnlyOwner
public class InvoiceResource {

    private static final Logger LOGGER = Logger.getLogger(InvoiceResource.class.getName());

    @EJB
    private AuthenticationService auth;

    @EJB
    private SubscriptionService sub;

    @EJB
    private InvoiceService invoiceService;

    @EJB
    private PlansService plan;



    @GET
    @Template(name = "invoices")
    @Produces({MediaType.TEXT_HTML})
    public TemplateContent getStatusHtml() throws FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/invoices (html)");
        TemplateContent<Map<String, Object>> content = new TemplateContent<>();
        Map<String, Object> value = new HashMap<>();
        value.put("profile", auth.getConnectedProfile());
        value.put("subscription", sub.connectedSubProfile());
        SubscriptionProfile subProfile = sub.connectedSubProfile();
        value.put("plan", plan.getPlanByEnum(subProfile.getPlan().toString()));
        value.put("invoices", invoiceService.getInvoicesByCustomerEmail(auth.getConnectedProfile().getEmail()));
        content.setContent(value);
        return content;
    }


}
