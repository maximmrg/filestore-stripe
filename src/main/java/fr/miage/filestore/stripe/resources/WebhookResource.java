package fr.miage.filestore.stripe.resources;

import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.net.Webhook;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.stripe.entities.StripeInvoice;
import fr.miage.filestore.stripe.services.CustomerService;
import fr.miage.filestore.stripe.services.InvoiceService;
import fr.miage.filestore.subscriptions.entities.EnumSubPlans;
import fr.miage.filestore.subscriptions.entities.Plan;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;
import fr.miage.filestore.subscriptions.service.PlansService;
import fr.miage.filestore.subscriptions.service.SubscriptionService;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("stripe")
@PermitAll
public class WebhookResource {

    private static final Logger LOGGER = Logger.getLogger(WebhookResource.class.getName());

    @EJB
    private PlansService plan;

    @EJB
    private SubscriptionService sub;

    @EJB
    private AuthenticationService auth;

    @EJB
    private InvoiceService invoiceService;

    @EJB
    private CustomerService customerService;

    @POST
    @Path("webhook")
    public Response webhook(String body, @Context HttpHeaders headers) throws StripeException {
        LOGGER.log(Level.INFO, "GET /api/stripe/webhook");

        //String endpointSecret = "whsec_5FxnEA7l7VvcDm6teSFDPAm7BvGfXIXC";
        String endpointSecret = "whsec_ed4c83a49741f5cbf18c66a3b8122ee2b9ab2997a22504010845b0461d2075a1";
        String stripeSignature = headers.getRequestHeader("Stripe-Signature").get(0);

        Event event = null;
        try {
            event = Webhook.constructEvent(
                    body, stripeSignature, endpointSecret
            );
        } catch (SignatureVerificationException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
        StripeObject stripeObject = null;
        if (dataObjectDeserializer.getObject().isPresent()) {
            stripeObject = dataObjectDeserializer.getObject().get();

        }

        System.out.println(stripeObject);

        switch (event.getType()) {
            case "invoice.paid":{
                fulfillOrder((Invoice)stripeObject);
                break;
            }
            case "customer.subscription.deleted":{
                removeSubscription((Subscription)stripeObject);
            }

            default:
                System.out.println("Unhandled event type: " + event.getType());
        }

        return Response.status(Response.Status.OK).build();
    }

    private void removeSubscription(Subscription stripeObject) {
        String customerId = stripeObject.getCustomer();
        String filestoreIdOfCustomer = customerService.getFilestoreIdOfCustomer(customerId);
        SubscriptionProfile profile = null;
        try {
            profile = sub.get(filestoreIdOfCustomer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert profile != null;
        sub.updateSubscription(profile.getEmail(), EnumSubPlans.FREE, false);
    }


    private void fulfillOrder(Invoice session) {
        String customerEmail = session.getCustomerEmail();
        String stripeProductId = session.getLines().getData().get(0).getPlan().getProduct();

        LOGGER.log(Level.INFO, "Retrieve plan by stripeId: " + stripeProductId);

        Plan p = plan.getByStripeId(stripeProductId);

        StripeInvoice invoice = new StripeInvoice();
        invoice.setId(session.getId());
        invoice.setCreationDate(new Date());
        invoice.setCharge_id(session.getCharge());
        invoice.setAmount(session.getAmountPaid());
        invoice.setProduct_id(stripeProductId);
        invoice.setCustomer_id(session.getCustomer());
        invoice.setCustomer_email(customerEmail);
        invoice.setHosted_invoice(session.getHostedInvoiceUrl());
        invoice.setInvoice_pdf(session.getInvoicePdf());

        invoiceService.saveInvoice(invoice);
        customerService.insertStripeCustomer(customerEmail, session.getCustomer());

        sub.updateSubscription(customerEmail, EnumSubPlans.valueOf(p.getPlan()), true);

    }

}
