package fr.miage.filestore.stripe.services;

import fr.miage.filestore.stripe.entities.StripeCustomer;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;
import fr.miage.filestore.subscriptions.service.SubscriptionService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class CustomerServiceBean implements CustomerService {

    private static final Logger LOGGER = Logger.getLogger(CustomerServiceBean.class.getName());

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;

    @EJB
    private SubscriptionService subscriptionService;

    @Override
    public ArrayList<StripeCustomer> getCustomers() {
        List<StripeCustomer> plans = em.createQuery("from StripeCustomer", StripeCustomer.class).getResultList();
        return new ArrayList<>(plans);
    }

    @Override
    public StripeCustomer getStripeCustomerByFilestoreId(String filestoreId) {
        System.out.println("getStripeCustomerByFilestoreId " + filestoreId);
        TypedQuery<StripeCustomer> q = em.createQuery("SELECT b FROM StripeCustomer b WHERE b.filestore_id = :filestore_id", StripeCustomer.class);
        q.setParameter("filestore_id", filestoreId);

        StripeCustomer sC = null;
        try{
             sC = q.getSingleResult();
        }catch(NoResultException e){

        }
        return sC;

    }

    @Override
    public void insertStripeCustomer(String email, String customerId){

        SubscriptionProfile subProfile = subscriptionService.getByEmail(email);
        String filestoreId = subProfile.getId();

        StripeCustomer sC = getStripeCustomerByFilestoreId(filestoreId);

        if(sC == null){
            StripeCustomer sCInsert = new StripeCustomer();
            sCInsert.setId(customerId);
            sCInsert.setFilestore_id(filestoreId);
            em.persist(sCInsert);
        }


    }

    @Override
    public String getFilestoreIdOfCustomer(String customerId) {
        TypedQuery<StripeCustomer> q = em.createQuery("SELECT b FROM StripeCustomer b WHERE b.id = :id", StripeCustomer.class);
        q.setParameter("id", customerId);
        return q.getSingleResult().getFilestore_id();
    }

}
