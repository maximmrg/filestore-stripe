package fr.miage.filestore.stripe.services;

import fr.miage.filestore.subscriptions.entities.Plan;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class StripeServiceBean implements StripeService {

    private static final Logger LOGGER = Logger.getLogger(StripeServiceBean.class.getName());

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;


    @Override
    public ArrayList<Plan> getPlans() {
        List<Plan> plans = em.createQuery("from Plan", Plan.class).getResultList();
        return new ArrayList<>(plans);
    }
}
