package fr.miage.filestore.stripe.services;

import fr.miage.filestore.stripe.entities.StripeCustomer;

import java.util.ArrayList;

public interface CustomerService {

    ArrayList<StripeCustomer> getCustomers();
    StripeCustomer getStripeCustomerByFilestoreId(String filestoreId);

    void insertStripeCustomer(String email, String customerId);

    String getFilestoreIdOfCustomer(String customerId);
}
