package fr.miage.filestore.stripe.services;

import fr.miage.filestore.stripe.entities.StripeInvoice;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.logging.Logger;

@Stateless
public class InvoiceServiceBean implements InvoiceService {

    private static final Logger LOGGER = Logger.getLogger(InvoiceServiceBean.class.getName());

    @PersistenceContext(unitName="fsPU")
    private EntityManager em;

    @Override
    public ArrayList<StripeInvoice> getInvoicesByCustomerEmail(String email) {
        TypedQuery<StripeInvoice> q = em.createQuery("SELECT b FROM StripeInvoice b WHERE b.customer_email = :email", StripeInvoice.class);
        q.setParameter("email", email);
        return new ArrayList<>(q.getResultList());
    }

    @Override
    public void saveInvoice(StripeInvoice invoice) {
        em.persist(invoice);
    }
}
