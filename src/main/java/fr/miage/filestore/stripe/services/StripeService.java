package fr.miage.filestore.stripe.services;

import fr.miage.filestore.subscriptions.entities.Plan;

import java.util.ArrayList;

public interface StripeService {

    ArrayList<Plan> getPlans();

}
