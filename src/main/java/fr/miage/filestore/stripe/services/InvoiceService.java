package fr.miage.filestore.stripe.services;

import fr.miage.filestore.stripe.entities.StripeInvoice;
import fr.miage.filestore.subscriptions.entities.Plan;

import java.util.ArrayList;

public interface InvoiceService {

    ArrayList<StripeInvoice> getInvoicesByCustomerEmail(String email);

    void saveInvoice(StripeInvoice invoice);

}
