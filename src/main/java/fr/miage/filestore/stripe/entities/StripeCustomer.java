package fr.miage.filestore.stripe.entities;

import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
public class StripeCustomer implements Serializable {

    @Id
    @JsonbProperty
    private String id;
    private String filestore_id;

    public StripeCustomer() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilestore_id() {
        return filestore_id;
    }

    public void setFilestore_id(String filestore_id) {
        this.filestore_id = filestore_id;
    }
}
