package fr.miage.filestore.stripe.entities;

import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
public class StripeInvoice implements Serializable {

    @Id
    @JsonbProperty
    private String id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    private String charge_id;
    private double amount;
    private String product_id;
    private String customer_id;
    private String customer_name;
    private String customer_email;
    private String hosted_invoice;
    private String invoice_pdf;

    public StripeInvoice() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharge_id() {
        return charge_id;
    }

    public void setCharge_id(String charge_id) {
        this.charge_id = charge_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getHosted_invoice() {
        return hosted_invoice;
    }

    public void setHosted_invoice(String hosted_invoice) {
        this.hosted_invoice = hosted_invoice;
    }

    public String getInvoice_pdf() {
        return invoice_pdf;
    }

    public void setInvoice_pdf(String invoice_pdf) {
        this.invoice_pdf = invoice_pdf;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
