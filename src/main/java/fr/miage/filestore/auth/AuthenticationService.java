package fr.miage.filestore.auth;

import fr.miage.filestore.auth.entity.Profile;
import fr.miage.filestore.subscriptions.entities.SubscriptionProfile;

import javax.persistence.TypedQuery;

public interface AuthenticationService {

    String UNAUTHENTIFIED_IDENTIFIER = "anonymous";

    boolean isAuthentified();

    String getConnectedIdentifier();

    Profile getConnectedProfile();

}
